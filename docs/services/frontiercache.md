
# Frontier Squid Cache

The Frontier distributed database caching system is common in the HEP comunity
as a way to distribute data to many clients around the world. 
The Frontier system uses the standard web caching tool squid to 
cache objects at every site, so that users access a cached copy much closer to their compute
job, and the downloading from the server only has to happen once even when running. 
The Frontier caches are often used to help with distobution of content from CVMFS

## Using the NERSC Frontier Cache

To use the frontier cache at NERSC you can include `frontiercache.nersc.gov` 
as part of your caching services.

## Technical details

At NERSC there are two caching servers `fs1.nersc.gov` and `fs2.nersc.gov` which are 
setup to be used round robin as `frontiercache.nersc.gov`. 
The servers are monitored using the 
[cern squid monitor](http://wlcg-squid-monitor.cern.ch/snmpstats/all.html), 
and can be found under with the name NERSC-Infra with the two coresponding servers
[fs1.nersc.gov](http://wlcg-squid-monitor.cern.ch/snmpstats/mrtgall/NERSC-Infra_fs1-ext.nersc.gov/index.html) 
and
[fs2.nersc.gov](http://wlcg-squid-monitor.cern.ch/snmpstats/mrtgall/NERSC-Infra_fs2-ext.nersc.gov/index.html).

The caching servers are physical servers with 64GB of RAM and 120GB of SSD allocated to the cache. 
Each server is running four workers each, with the cache divided equally among the workers.
These servers have 25Gb NICs for connecting to the HPC environment for compute related work,
and 10Gb NICs for connecting with the outside world for downloading data.
