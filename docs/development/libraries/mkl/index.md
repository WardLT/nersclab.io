# MKL

The [Intel Math Kernel Library](https://software.intel.com/en-us/mkl)
(Intel MKL) contains highly optimized, extensively threaded math
routines for science, engineering, and financial applications. Core
math functions include BLAS, LAPACK, ScaLAPACK, Sparse Solvers, Fast
Fourier Transforms, Vector Math, and more.

## Usage on Perlmutter

### Intel compilers

When using the Intel compilers under PrgEnv-intel, MKL can be conveniently
used by adding the `-mkl` flag to the compiler line.

```bash
nersc$ module load PrgEnv-intel
nersc$ ftn -qmkl test.f90
nersc$ cc -qmkl test.c
```

If you need the sequential MKL routines only then use

```bash
nersc$ ftn -qmkl="sequential" test.f90
nersc$ cc -qmkl="sequential" test.c
```

If ScaLapack routines are needed then

```bash
nersc$ ftn -qmkl="cluster" test.f90
nersc$ cc -qmkl="cluster" test.c
```

### Other compilers

Use the
[Intel MKL Link Line Advisor](https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor) to
determine the appropriate link lines.

## Preparing for Perlmutter

For more info on alternatives to MKL libraries on Perlmutter, please see the [Perlmutter
Readiness page](../../../performance/readiness.md/#alternatives-to-intel-math-kernel-library-mkl-for-cpu-math-functions).
